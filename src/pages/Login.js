import React, { useState } from "react";
import { HiOutlineMail } from "react-icons/hi";
import "../styles/Login.scss";
import { BiLock } from "react-icons/bi";
import { BsFillEyeFill, BsFillEyeSlashFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";

const Login = () => {
	const token = "Login";
	const [showPass, setShowPass] = useState(false);
	const navigate = useNavigate();
	const login = () => {
		localStorage.setItem(token, "Successfully Logged In.");
		navigate("/");
	};

	return (
		<form
			className="login"
			onSubmit={(e) => {
				e.preventDefault();
				login();
			}}
		>
			<div className="login--heading">Login</div>
			<div className="login--inputs">
				<div className="input_default">
					<HiOutlineMail className="icon" />
					<input
						type="email"
						className=""
						required
						name="email"
						placeholder="Email"
					/>
				</div>
				<div className="input_default">
					<BiLock className="icon" />
					<input
						type={showPass === true ? "text" : "password"}
						className=""
						placeholder="Password"
						required
					/>
					{showPass === true ? (
						<BsFillEyeSlashFill
							className="icon"
							required
							onClick={() => setShowPass(false)}
						/>
					) : (
						<BsFillEyeFill className="icon" onClick={() => setShowPass(true)} />
					)}
				</div>
				<button type="submit" className="input_btn">
					<span>Submit</span>
				</button>
			</div>
		</form>
	);
};

export default Login;
