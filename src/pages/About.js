import React from "react";
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import aboutImg from "../assets/about.jpg";
import Helmet from "react-helmet";

const About = () => {
	return (
		<div>
			<Helmet>
				<meta charSet="utf-8" />
				<title>About</title>
				<meta
					name="description"
					content="this is the home page of traveller-v2s"
				/>
			</Helmet>
			<Nav />
			<Banner
				img={aboutImg}
				heading="ABOUT US"
				desc="Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
				corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
				nemo enim, gravida lobortis quasi, eum."
			/>
		</div>
	);
};

export default About;
