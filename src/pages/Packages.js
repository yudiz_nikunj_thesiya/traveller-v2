import React from "react";
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import PopularDestination from "../components/PopularDestination";
import packageImg from "../assets/packages.jpg";

const Packages = () => {
	return (
		<div>
			<Nav />
			<Banner
				img={packageImg}
				heading="PACKAGES"
				desc="Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
				corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
				nemo enim, gravida lobortis quasi, eum."
			/>
		</div>
	);
};

export default Packages;
