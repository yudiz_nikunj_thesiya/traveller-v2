import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import data from "../data/destinations.json";

const Destination = () => {
	const [destination, setDestination] = useState([]);
	const { id } = useParams();
	useEffect(() => {
		setDestination(data.data.filter((curr) => curr.id == id));
	}, []);
	return (
		<div>
			<Nav />
			{destination.map((curr) => (
				<Banner
					img={curr.destinationImg}
					heading={`${curr.label}, ${curr.title}`}
					// 	desc="Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
					// corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
					// nemo enim, gravida lobortis quasi, eum."
				/>
			))}
		</div>
	);
};

export default Destination;
