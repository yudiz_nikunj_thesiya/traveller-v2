import React from "react";
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import destinationsImg from "../assets/destination.jpg";
import PopularDestination from "../components/PopularDestination";

const Destinations = () => {
	return (
		<div>
			<Nav />
			<Banner
				img={destinationsImg}
				heading="DESTINATIONS"
				desc="Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
				corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
				nemo enim, gravida lobortis quasi, eum."
			/>
			<PopularDestination />
		</div>
	);
};

export default Destinations;
