import React from "react";
import HeroSection from "../components/HeroSection";
import Inquiry from "../components/Inquiry";
import Nav from "../components/Nav";
import PopularDestination from "../components/PopularDestination";
import "../styles/home.scss";
import { Helmet } from "react-helmet";

const Home = () => {
	return (
		<div className="home">
			<Helmet>
				<meta charSet="utf-8" />
				<title>Home</title>
				<meta
					name="description"
					content="this is the home page of traveller-v2s"
				/>
				<meta
					name="google-site-verification"
					content="r0AUxZI5sphlifCLiVgyzcKd4lKlZXfLjcONYxmuEQ4"
				/>
			</Helmet>
			<Nav />
			<HeroSection />
			<Inquiry />
			<PopularDestination />
		</div>
	);
};

export default Home;
