import React from "react";
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import contactImg from "../assets/dubai.png";

const Contact = () => {
	return (
		<div>
			<Nav />
			<Banner
				img={contactImg}
				heading="CONTACT US"
				desc="Ac mi duis mollis. Sapiente? Scelerisque quae, penatibus? Suscipit class
				corporis nostra rem quos voluptatibus habitant? Fames, vivamus minim
				nemo enim, gravida lobortis quasi, eum."
			/>
		</div>
	);
};

export default Contact;
