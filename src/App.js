import Home from "./pages/Home";
import { Routes, Route } from "react-router-dom";
import About from "./pages/About";
import Destinations from "./pages/Destinations";
import Packages from "./pages/Packages";
import Contact from "./pages/Contact";
import Destination from "./pages/Destination";
import Login from "./pages/Login";
import PrivateRoute from "./components/PrivateRoute";
import PublicRoute from "./components/PublicRoute";

function App() {
	return (
		<div className="App">
			<Routes>
				<Route path="/" element={<PrivateRoute />}>
					<Route path="/" element={<Home />} />
					<Route path="/about" element={<About />} />
					<Route path="/destinations" element={<Destinations />} />
					<Route path="/destination/:id" element={<Destination />} />
					<Route path="/packages" element={<Packages />} />
					<Route path="/contact" element={<Contact />} />
				</Route>
				<Route
					path="/login"
					element={
						<PublicRoute>
							<Login />
						</PublicRoute>
					}
				/>
			</Routes>
		</div>
	);
}

export default App;
