import React from "react";
import { Navigate, Outlet, Route, useNavigate } from "react-router-dom";
import { isLoggedIn } from "./isLoggedIn";

const PrivateRoute = ({ children }) => {
	return isLoggedIn() === true ? <Outlet /> : <Navigate to="/login" />;
};

export default PrivateRoute;
