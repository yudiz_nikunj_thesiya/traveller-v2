import React from "react";
import "../styles/inquiry.scss";

const Inquiry = () => {
	return (
		<div className="inquiry">
			<div className="inquiry__search">
				<span>Search Destination</span>
				<input type="text" placeholder="Enter Destination.." />
			</div>
			<div className="inquiry__pax">
				<span>Pax Number</span>
				<input type="number" placeholder="No. of people.." />
			</div>
			<div className="inquiry__date">
				<span>Checkin Date</span>
				<input type="date" />
			</div>
			<div className="inquiry__date">
				<span>Checkout Date</span>
				<input type="date" />
			</div>
			<button>INQUIRY NOW</button>
		</div>
	);
};

export default Inquiry;
