import React from "react";
import "../styles/banner.scss";

const Banner = ({ heading, img, desc }) => {
	var sectionStyle = {
		background: `linear-gradient(to left, rgba(20, 20, 20, 0.74), rgba(20, 20, 20, 0.76)), url(${img})`,
		backgroundPosition: "center center",
		backgroundSize: "cover",
		backgroundRepeat: "no-repeat",
	};
	return (
		<div className="banner" style={sectionStyle}>
			<div className="banner__heading">{heading}</div>
			<span className="banner__desc">{desc}</span>
		</div>
	);
};

export default Banner;
