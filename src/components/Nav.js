import React, { useState } from "react";
import logo from "../assets/logo.png";
import "../styles/nav.scss";
import { RiMenu3Line } from "react-icons/ri";
import "../styles/verticalnav.scss";
import { MdOutlineClose } from "react-icons/md";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { logout } from "./isLoggedIn";

const Nav = () => {
	const [mobileNav, setMobileNav] = useState(false);
	const navigate = useNavigate();
	return (
		<div className="container">
			<div className="nav">
				<NavLink
					to="/"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					<img src={logo} alt="logo" className="" />
				</NavLink>
				<div className="nav__menu">
					<NavLink
						to="/"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						HOME
					</NavLink>
					<NavLink
						to="/about"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						ABOUT US
					</NavLink>
					<NavLink
						to="/destinations"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						DESTINATIONS
					</NavLink>
					<NavLink
						to="/packages"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						PACKAGES
					</NavLink>
					<NavLink
						to="/contact"
						className={({ isActive }) => (isActive ? "active" : "inactive")}
					>
						CONTACT
					</NavLink>
				</div>
				<div className="nav__btns">
					<button
						className="nav__btns--btn-2"
						onClick={() => {
							logout();
							navigate("/login");
						}}
					>
						Logout
					</button>
					<button
						className="nav__btns--btn-1"
						onClick={() => setMobileNav(true)}
					>
						<RiMenu3Line />
					</button>
				</div>
			</div>
			{mobileNav && (
				<div className="verticalnav">
					<div className="verticalnav__header">
						<img src={logo} alt="logo" className="verticalnav__header--logo" />
						<button
							className="verticalnav__header--btn"
							onClick={() => setMobileNav(false)}
						>
							<MdOutlineClose />
						</button>
					</div>
					<div className="verticalnav__menu">
						<span>
							<NavLink
								to="/"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								HOME
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/about"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								ABOUT US
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/destinations"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								DESTINATIONS
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/packages"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								PACKAGES
							</NavLink>
						</span>
						<span>
							<NavLink
								to="/contact"
								className={({ isActive }) => (isActive ? "active" : "inactive")}
								onClick={() => setMobileNav(false)}
							>
								CONTACT
							</NavLink>
						</span>
						<span
							className="verticalnav-btn"
							onClick={() => {
								logout();
								navigate("/login");
							}}
						>
							LOGOUT
						</span>
					</div>
				</div>
			)}
		</div>
	);
};

export default Nav;
