import React, { useEffect, useState } from "react";
import DestinationCard from "./DestinationCard";
import "../styles/populardestination.scss";
import data from "../data/destinations.json";

const PopularDestination = () => {
	const [info, setInfo] = useState([]);
	useEffect(() => {
		setInfo(data.data);
	}, []);
	return (
		<div className="populardestination">
			<span className="populardestination__label">UNDERCOVER PLACE</span>
			<span className="populardestination__heading">POPULAR DESTINATION</span>
			<span className="populardestination__desc">
				Fusce hic augue velit wisi quibusdam pariatur, iusto primis, nec nemo,
				rutrum. Vestibulum cumque laudantium. Sit ornare mollitia tenetur,
				aptent.
			</span>
			<div className="card-container">
				{info.map((destination, index) => (
					<DestinationCard
						key={index}
						img={destination.img}
						label={destination.label}
						heading={destination.title}
						desc={destination.shortDesc}
						id={index}
					/>
				))}
			</div>
		</div>
	);
};

export default PopularDestination;
