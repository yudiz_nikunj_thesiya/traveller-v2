import React from "react";
import "../styles/populardestination.scss";
import { BsStarFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";

const DestinationCard = ({ img, label, heading, desc, id }) => {
	const navigate = useNavigate();
	const clickHandle = () => {
		navigate(`/destination/${id + 1}`);
	};
	return (
		<div className="card" onClick={clickHandle}>
			<img src={img} alt="house" className="card__image" />
			<div className="card__details">
				<span className="card__details--label">{label}</span>
				<div className="card__details--star">
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
					<BsStarFill />
				</div>

				<span className="card__details--heading">{heading}</span>
				<span className="card__details--desc">{desc}</span>
			</div>
		</div>
	);
};

export default DestinationCard;
