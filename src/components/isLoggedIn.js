import { Navigate } from "react-router-dom";

const token = "Login";

export const login = () => {
	localStorage.setItem(token, "Successfully Logged In.");
};

export const logout = () => {
	localStorage.removeItem(token);
};

export const isLoggedIn = () => {
	if (localStorage.getItem(token)) {
		return true;
	}
	return false;
};
